import os
import re
import sys
from jinja2 import Template

files = os.walk('.')

names = files.next()[1]
names.remove('.git')

packages = {}

for name in names:
    packages[name] = {}

    pkgfile = open(name + "/Pkgfile", "r")

    for line in pkgfile:
        if re.search("URL", line):
             packages[name]['url'] = line.split()[2]
        if re.search("Description", line):
            packages[name]['description'] = " ".join(line.split()[2:])
        if re.search("^version", line):
            packages[name]['version'] = line.split("=")[1]
    packages[name]['files'] = os.walk(name).next()[2]


template = Template("""
<html>
    <head>
        <title>ports</title>
        <link rel="stylesheet" type="text/css" href="main.css">
    </head>

    <body>
        <div id="pad">
            <div id="contain">
                <div class="nav">
                    <a href="https://maraku.xyz"> home </a>
                    <a href="https://maraku.xyz/projects.html"> projects </a>
                    <a href="https://maraku.xyz/ports.html"> ports </a>
                    <a href="https://maraku.xyz/about.html"> about </a>
                </div>
            </div>
        </div>

        <div id="pad">
            <div id="contain">
                <h1> ports </h1>
                <h3> sync files </h3>
                <blockquote>
                    <a href='maraku.httpup'>maraku.httpup</a> <br>
                    <a href='maraku.git'>maraku.git</a> <br>
                    <a href='maraku.pub'>maraku.pub</a>
                </blockquote>
                <table>
                <tr>
                    <td>port</td>
                    <td>version</td>
                    <td>files</td>
                    <td>description</td>
                </tr>

                {% for name, misc in packages %}
                <tr>
                    <td> <a href='{{misc['url']}}'> {{name}} </a> </td>
                    <td> {{misc['version']}} </td>
                    <td>
                        {% for filename in misc['files'] %}
                                <a href='{{name}}/{{filename}}'> - {{filename}} </a><br>
                        {% endfor %}
                    <td> {{misc['description']}} </td>
                </tr>
                {% endfor %}

            </div>
        </div>
    </body>
</html>""")
print(template.render(packages=sorted(packages.items())));
